import axios from 'axios';

const API_KEY = '73f16834bdae4a5ff574378eb743d20c';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

// Constant declared type action
export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city},ie`;
  const request = axios.get(url);

  // the promise is returned as the payload, request is the promise here
  return {
    type: FETCH_WEATHER,
    payload: request
  };
}