import React, { Component } from 'react';

class GoogleMap extends Component {
  // ComponentDidMount is called after the component is rendered to the screen
  componentDidMount() {
    new google.maps.Map(this.refs.map, {
      zoom: 12,
      center: {
        lat: this.props.lat,
        lng: this.props.lon
      }
    });
  }

  render() {
    // Here gives you reference to this.refs.map anywhere in the app
    return <div ref="map" />
  }
}

export default GoogleMap;